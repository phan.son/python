#!/usr/bin/env python
# coding: utf-8

# # K-Nearest Neighbor Algorithmus:
# 
# #### Parameter:  
# 
# dataset = (X, Y)  
# mit X := Features  
# und Y := Classes  
# 
# K := Hyperparameter für die nächsten k Nachbarn  
# sample := Neuer Datenpunkt zum Klassifizieren 
# 
# #### Pseudo-Code:
# 
# kNN (dataset, K, sample):  
#   - Bestimme von jedem Punkt $p \in dataset$ die Distanz, mit der geg. Distanzfunktion.
#   - Bestimme die $K$ nächst gelegenen Nachbarn und bestimme die zugehörige Klasse von $sample$.  

# In[186]:


import numpy as np
import matplotlib.pyplot as plt


# In[187]:


def generate_dataset():
    cov = np.array([[1, 0], [0, 1]])
    data1 = np.random.multivariate_normal(np.array([9, 0]), cov, 10)
    data2 = np.random.multivariate_normal(np.array([-10, 4]), cov, 6)
    data3 = np.random.multivariate_normal(np.array([-10, 10]), cov, 13)
    data = np.concatenate((data1, data2, data3), axis=0)
    classes = np.array([0 for i in range(10)] + [1 for i in range(6)] + [2 for i in range(13)])
    return data, classes


# In[188]:


dataset, classes = generate_dataset()


# In[189]:


print(dataset.shape)
print(classes.shape)
print("Sample: x[0], y[0]")
print(dataset)
print(classes)


# In[190]:


def plot_dataset(dataset, classes):
    colors = ["red", "blue", "green"]
    for index, point in enumerate(dataset):
        print(index, point)
        print(classes[index])
        plt.scatter(point[0], point[1], color=colors[classes[index]])
    plt.show()


# In[191]:


def distance(p1, p2):
    # Euklid
    distance = np.linalg.norm(p1 - p2)
    return distance


# In[288]:


def vote(neighbours, num_classes):
    #vote = [0 for i in range(num_classes)]
    vote = np.zeros(num_classes)
    for neigh in neighbours:
        cl = neigh[1]
        vote[cl] += 1
    maxi = max(vote)
    idx =np.argwhere(vote==maxi)
    if idx.shape != 1:
        distance = [neighbours[i][0] for i in range(num_classes)]
        print(distance)
        temp = np.argmin(distance)
        voted_class = neighbours[temp][1]
    else:
        voted_class = np.argmax(vote) 
    return voted_class


# In[203]:


def KNN(dataset, classes, num_classes, K, sample):
    num_samples = dataset.shape[0]
    num_features = dataset.shape[1]
    # List with distances from sample to dataset
    neighbours = sorted([(distance(sample, dataset[i]), classes[i], dataset[i]) for i in range(num_samples)])
    neighbours = neighbours[:K]
    voted_class = vote(neighbours, num_classes)
    return voted_class, neighbours


# In[289]:


K = 3
sample = np.array([0, 6])
num_classes = np.unique(classes).shape[0]

voted_class, neighbours = KNN(dataset, classes, num_classes, K, sample)
# Distance, Class, Coordinate
print(neighbours)
print(voted_class)


# In[279]:


def plot_knn(dataset, classes, sample, voted_class, neighbours):
    colors = ["red", "blue", "green"]
    for index, point in enumerate(dataset):
        plt.scatter(point[0], point[1], color=colors[classes[index]])
    plt.scatter(sample[0], sample[1], color=colors[voted_class])
    for neigh in neighbours:
        cl = neigh[1]
        p = neigh[2]
        #plt.scatter(p[0], p[1], color="yellow", s=100)
        plt.plot((p[0], sample[0]), (p[1], sample[1]), color='cyan')
    plt.show()


# In[290]:


plot_knn(dataset, classes, sample, voted_class, neighbours)

